import pandas as pd
coffee_data = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";")

# TYPE OF FOOD AND DRINKS AND NUMBER OF CUSTOMERS
print("Here are the food available: " + str(coffee_data['FOOD'].unique()))
print("Here are the drinks available: " + str(coffee_data['DRINKS'].unique()))
print("There are " + str(len(coffee_data['CUSTOMER'].unique())) + " unique customers")

# PLOT 1
total_food = pd.value_counts(coffee_data["FOOD"])
total_food.plot(kind='bar', title='Amount of each type of food')

# PLOT 2
total_drink = pd.value_counts(coffee_data["DRINKS"])
total_drink.plot(kind='bar', title='Amount of each type of drink', colormap='BuGn_r')

# AVERAGE FOOD/DRINKS
copy_data = coffee_data.copy(deep=True)
copy_data['DATE'], copy_data['HOUR'] = copy_data['TIME'].str.split(" ").str
copy_data['FOOD'] = copy_data['FOOD'].fillna("Null")

food_proba = copy_data[['HOUR', 'FOOD']].groupby(['HOUR', 'FOOD']).size()
drink_proba = copy_data[['HOUR', 'DRINKS']].groupby(['HOUR', 'DRINKS']).size()

# REORGANISE THE COLUMNS
food_proba, drink_proba = food_proba.reset_index(), drink_proba.reset_index()
food_proba.columns = ['HOUR', 'FOOD', 'PROBABILITY']
drink_proba.columns = ['HOUR', 'DRINKS', 'PROBABILITY']

# CALCULATE THE PROBABILITY
total_daily_food = food_proba[['HOUR', 'PROBABILITY']].groupby('HOUR').sum()
total_daily_drink = drink_proba[['HOUR', 'PROBABILITY']].groupby('HOUR').sum()
food_proba['PROBABILITY'] = (food_proba['PROBABILITY'] / 1825 * 100)
drink_proba['PROBABILITY'] = (drink_proba['PROBABILITY'] / 1825 * 100)

# SAVE THE PROBABILITIES AND THE CHANGED DATA
drink_proba.to_csv("../Data/drink_proba.csv", sep=";", index=False)
food_proba.to_csv("../Data/food_proba.csv", sep=";", index=False)
