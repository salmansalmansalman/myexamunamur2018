import pandas as pd

coffee_data = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";")

#Returning customers of coffee_data
customers = []
coffee_data['DATE'], coffee_data['HOUR'] = coffee_data['TIME'].str.split(" ").str
for i in coffee_data['CUSTOMER']:
    customers.append(i)
frequency = {}.fromkeys(set(customers), 0)  # create a dictionary with id as a key
for time in customers:
    frequency[time] += 1  # set the number of orders for each returning customers
for key in frequency.copy():  # copy used otherwise the length of the dictionary changes
    if frequency[key] <= 1:
        del frequency[key]
print("There are %s returning customers " % len(frequency))

# Specific time
returning_customers_dataframe = coffee_data[coffee_data['CUSTOMER'].isin(frequency)][['HOUR', 'CUSTOMER', 'DRINKS', 'FOOD']]
specific_time_orders = returning_customers_dataframe.groupby(['CUSTOMER','HOUR']).size()
specific_time_orders_dico = specific_time_orders.to_dict()
for id_time, order in specific_time_orders_dico.items():
    if order >= 6:  # Let's imagine that going 6 times at the same time shows a habit
        print("The customer %s came at least 6 times at %s" % (id_time[0], id_time[1]))
