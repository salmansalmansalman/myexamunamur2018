import random
import numpy as np


class customer(object):
    def __init__(self, id):
        self.id = id
        self.budget = 100
        self.history = {}

    def select_drink(self, hour, drink_proba):
        probabilities = []
        drinks = []
        for key in drink_proba:
            if key[0] == hour:  # get drinks and probabilities associated to the specific time
                probabilities.append(drink_proba[key][0])
                drinks.append(key[1])
        drink = np.random.choice(drinks, 1, probabilities)[0]
        return drink

    def select_food(self, hour, food_proba):
        probabilities = []
        food = []
        for key in food_proba:
            if key[0] == hour:
                probabilities.append(food_proba[key][0])
                food.append(key[1])
        piece_of_food = np.random.choice(food, 1, probabilities)[0]
        return piece_of_food

    def order(self, date, hour, drink_proba, food_proba, prices):
        drink = self.select_drink(hour, drink_proba)  # return of previous method
        self.budget -= prices[drink]  # reduce the budget
        piece_of_food = self.select_food(hour, food_proba)
        self.budget -= prices[piece_of_food]
        data = [date, hour, self.id, drink, piece_of_food]  # data to add to the dictionary
        self.history[date, hour] = [drink, piece_of_food, prices[drink] + prices[piece_of_food], self.budget]
        return data


class onetime_customer(customer):
    """ no overriding methods"""


class regular__onetime_customer(onetime_customer):
    """ no overriding methods"""


class tripadvisor_customer(onetime_customer):

    def order(self, date, hour, drink_proba, food_count, prices):
        drink = self.select_drink(hour, drink_proba)
        self.budget -= prices[drink]
        piece_of_food = self.select_food(hour, food_count)
        self.budget -= prices[piece_of_food]
        data = [date, hour, self.id, drink, piece_of_food]
        tip = random.randint(1, 10)
        self.budget -= tip  # reduce the tip from the budget
        self.history[date, hour] = [drink, piece_of_food, prices[drink] + prices[piece_of_food] + tip, self.budget]  # add tip in history
        return data


class returning_customer(customer):

    def order(self, date, hour, drink_proba, food_proba, prices):
        drink = self.select_drink(hour, drink_proba)
        piece_of_food = self.select_food(hour, food_proba)
        if self.budget - prices[drink] >= 0:  # customer can buy a drink
            self.budget -= prices[drink]
            if self.budget - prices[piece_of_food] >= 0:  # customer can buy a drink and food
                self.budget -= prices[piece_of_food]
                data = [date, hour, self.id, drink, piece_of_food]
                self.history[date, hour] = [drink, piece_of_food, prices[drink] + prices[piece_of_food], self.budget]
            else:  # customer only buys a drink
                data = [date, hour, self.id, drink, "Null"]
                self.history[date, hour] = [drink, "Null", prices[drink], self.budget]
        else:  # customer buys nothing
            data = [date, hour, "Not enough budget", drink, piece_of_food]
        return data


class regular_returning_customer(returning_customer):

    def __init__(self, id):
        returning_customer.__init__(self, id)
        self.budget = 250  # change budget


class hipster_customer(returning_customer):

    def __init__(self, id):
        returning_customer.__init__(self, id)
        self.budget = 500  # change budget
