from Code.Part2 import *
import random as r
import pandas as pd

drink_proba = pd.read_csv("../Data/drink_proba.csv", sep=";")
drink_proba = drink_proba.set_index(['HOUR', 'DRINKS']).T.to_dict('list')
food_proba = pd.read_csv("../Data/food_proba.csv", sep=";")
food_proba = food_proba.set_index(['HOUR', 'FOOD']).T.to_dict('list')
coffee_data = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";")  # pas utile je crois
prices = {'milkshake': 5, 'frappucino': 4, 'water': 2, 'coffee': 3, 'soda': 3, 'tea': 3,
          'sandwich': 5, 'cookie': 2, 'pie': 3, 'muffin': 3, 'Null': 0}  # prices
for key in prices:
    prices[key] *= 1.2

returning_customer_list = []
for i in range(1000):
    decision = r.randint(1, 3)
    if decision != 3:
        returning_customer_list.append(regular_returning_customer(str.zfill(str(i), 8)))
    else:
        returning_customer_list.append(hipster_customer(str.zfill(str(i), 8)))
#time = list(copy_data['TIME'])
dico_simulation = {}
customer_list = []
copy_returning_list = returning_customer_list.copy()
max_returning_customer = 999
for time in coffee_data['TIME']:
    decision = r.randint(1, 100)
    customer = ""
    try:
        if decision <= 20:
            customer = returning_customer_list[r.randint(0, max_returning_customer)]
        elif 21 <= decision <= 28:
            customer = tripadvisor_customer(str.zfill(str(r.randint(1000, 90000000)), 8))
        else:
            customer = regular__onetime_customer(str.zfill(str(r.randint(1000, 90000000)), 8))
        customer_list.append(customer)
        dico_simulation[time[0:10], time[11:]] = customer.order(time[0:10], time[11:], drink_proba, food_proba, prices)
        if dico_simulation[time[0:10], time[11:]][2] == "Not enough budget":
            returning_customer_list.remove(customer)
            max_returning_customer -= 1
    except ValueError:
        pass

dataframe_simulation = pd.DataFrame(columns=['DATE', 'HOUR', 'CUSTOMER', 'DRINKS', 'FOOD'])
dataframe_simulation = dataframe_simulation.from_dict(dico_simulation, orient='index')
dataframe_simulation.reset_index(inplace=True, drop=True)
dataframe_simulation.columns = ['DATE', 'HOUR', 'CUSTOMER', 'DRINKS', 'FOOD']
